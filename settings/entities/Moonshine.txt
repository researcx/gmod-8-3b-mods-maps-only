"Moonshine"
{
	"classname"			"prop_physics"
	"type"				"prop"
	"keyvalues"
	{
	"model" "models/props_junk/glassjug01.mdl"
	"health" "10"
	"rendercolor" "255 255 0"

	"OnBreak" "!activator,addoutput,gravity 0.1"
	"OnBreak" "!activator,addoutput,gravity 1,60"
	"OnBreak" "!activator,color,255+225+225"
	"OnBreak" "!activator,color,255+255+255,60"
	}
}