"Jeep"
{
	"classname"			"prop_physics_override"
	"type"				"prop"
	"keyvalues"
	{
	"spawnflags" "500"
	"physdamagescale" "1.1"
	"inertiaScale" "1.0"
	"fademindist" "-1"
	"fadescale" "1"
	"model" "models/props_junk/GlassBottle01a.mdl"
	"skin" "0"
	"massScale" "0"
	"ExplodeDamage" "100"
	"ExplodeRadius" "2000"
	"minhealthdmg" "0"
	"health" "10"
	"rendercolor" "0 255 0"
	}
}