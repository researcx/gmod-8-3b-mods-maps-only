"Jeep"
{
	"classname"			"prop_physics_override"
	"type"				"prop"
	"keyvalues"
	{
	"spawnflags" "256"
	"physdamagescale" "0.1"
	"inertiaScale" "1.0"
	"fademindist" "-1"
	"fadescale" "1"
	"model" "models/props_junk/propane_tank001a.mdl"
	"skin" "0"
	"massScale" "0"
	"ExplodeDamage" "100"
	"ExplodeRadius" "150"
	"minhealthdmg" "0"
	"health" "1"
	"rendercolor" "0 255 0"
	}
}