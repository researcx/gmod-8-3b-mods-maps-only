"Jeep"
{
	"classname"			"npc_maker"
	"type"				"effect"
	"keyvalues"
	{
	"targetname" "npcspawner"
	"MaxNPCCount" "1"
	"SpawnFrequency" "20"
	"MaxLiveChildren" "-1"
	"NPCType" "npc_antlionguard"
	"NPCTargetname" "spawnedenemy"
	"spawnflags" "48"
	"startdisabled" "1"
	"additionalequipment" ""
	}
}