"Jeep"
{
	"classname"			"npc_maker"
	"type"				"effect"
	"keyvalues"
	{
	"targetname" "npcspawner"
	"MaxNPCCount" "1"
	"SpawnFrequency" "5"
	"MaxLiveChildren" "-1"
	"NPCType" "npc_fastzombie"
	"NPCTargetname" "spawnedenemy"
	"spawnflags" "48"
	"startdisabled" "1"
	"additionalequipment" ""
	}
}