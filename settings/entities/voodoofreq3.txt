"voodoofreq3"
{
	"classname"			"prop_physics"
	"type"				"prop"
	"keyvalues"
	{

	"model" "models/props_c17/doll01.mdl"
	"rendercolor" "100 100 200"
	"MaterialOverride" "models/combine_advisor/body9"

	"physdamagescale" "100"

	"onbreak" "cursedfreq3,clearparent"
	"onbreak" "cursedfreq3,sethealth,0,13"
	"onbreak" "cursedfreq3,addoutput,targetname player"

	"onhealthchanged" "cursedfreq3,sethealth,100"
	"onhealthchanged" "cursedfreq3,sethealth,13,0.01"

	"onphysgunpickup" "cursedfreq3,addoutput,gravity 0"
	"onphysgundrop" "cursedfreq3,addoutput,gravity 100"
	"onphysgundrop" "cursedfreq3,addoutput,gravity 1,1"

	"onuser1" "cursedfreq3,ignite"
	"onuser1" "!self,color,255+150+0"
	"onuser1" "!self,color,100+100+200,30"

	"onuser2" "cursedfreq3,addoutput,health 0"
	"onuser2" "!self,color,0+0+0"
	"onuser2" "!self,disablemotion"
	"onuser2" "!self,color,100+100+200,0.5"

	"onmotionenabled" "cursedfreq3,addoutput,health 100"

	"onuser3" "cursedfreq3,sethealth,0"
	"onuser3" "!self,color,255+0+0"
	"onuser3" "!self,color,200+0+0,0.5"
	"onuser3" "!self,color,150+0+0,1"
	"onuser3" "!self,color,100+0+0,1.5"
	"onuser3" "!self,color,50+0+0,2"
	"onuser3" "!self,color,0+0+0,2.5"
	"onuser3" "!self,color,100+100+200,3"

	"onuser4" "cursedfreq3,addoutput,gravity 100"
	"onuser4" "cursedfreq3,addoutput,gravity 1,13"
	"onuser4" "!self,color,100+100+100"
	"onuser4" "!self,color,100+100+200,13"


	}
}