GMOW V3

Credits:

TRW: Modelled: Bmw m3, Jeep cj7, and monster jeep yj
Gplex: Gp_City, GP_Offroad, GP_Airboatrace, GP_Highway, GP_Racetrack, Trackmania

Onslaught map originally created by shanjaq, modified by gplex
Everything else made by Solid_Granite
-------------------------------------
Install Instructions

1:Extract contents of RAR to gmod\mods
2:Delete gmod\mods\gmow2 folder

When you next load the game, it should automatically rebuild the
modcache. Note this will probably take 5 mins or more.


NOTE
There is a problem where the surfaceproperties.txt is edited in
more than one mod (this mod, phoenix model pack and maybe some
others). But the modcache system only allows for one of said file.

GMOW uses this to give the cars' wheels a lot of grip, if another
mod's surfaceproperties.txt is chosen, gmow v3 will still work,
but the wheels will be very slippery.

So either edit gmod\mods\-modcache\scripts\surfaceproperties.txt 
and put in the unique entries for all mods that use it, or choose
which mod will have its own surfaceproperties.txt and copy that
across to modcache.
-------------------------------------------------------------
