"PHX [TRAINS]"
 {
	"~USABLE Trains" ""
	"=FSD-OverRun"   			"train"

	"~NOT USABLE Trains" ""
	"FS-OverRun"                		"models\props_phx\trains\fsd-overrun.mdl"

	"~Monorails" ""
	"Monorail 1x"               	 	"models\props_phx\trains\monorail1.mdl"
	"Monorail 2x"               	 	"models\props_phx\trains\monorail2.mdl"
	"Monorail 4x"                		"models\props_phx\trains\monorail3.mdl"
	"Monorail 8x"                		"models\props_phx\trains\monorail4.mdl"
	"Monorail Arch 90�"                	"models\props_phx\trains\monorail_curve.mdl"
	"Monorail Arch 45�"                	"models\props_phx\trains\monorail_curve2.mdl"

	"~RAILROADS!" ""
	"RailRoad Single"                	"models\props_phx\trains\tracks\track_single.mdl"
	"RailRoad 1x"                	"models\props_phx\trains\tracks\track_1x.mdl"
	"RailRoad 2x"                	"models\props_phx\trains\tracks\track_2x.mdl"
	"RailRoad 4x"                	"models\props_phx\trains\tracks\track_4x.mdl"
	"RailRoad 8x"                	"models\props_phx\trains\tracks\track_8x.mdl"
	"RailRoad 16x"                	"models\props_phx\trains\tracks\track_16x.mdl"
	"RailRoad Turn90"              	"models\props_phx\trains\tracks\track_turn90.mdl"
	"RailRoad Turn45"              	"models\props_phx\trains\tracks\track_turn45.mdl"
	"RailRoad Crossing              	"models\props_phx\trains\tracks\track_crossing.mdl"
	
	"Train Wheel"                	"models\props_phx\trains\wheel_medium.mdl"
	"Wheel Base"                	"models\props_phx\trains\wheel_base.mdl"
}
