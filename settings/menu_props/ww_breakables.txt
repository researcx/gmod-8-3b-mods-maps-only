"WoodWars stuff"
{
	"=Armorshard"                	"gmp_ww/armorshard"
	"~Explosives"	""
	"=Cannister"                		"cannister"
	"=Bottle"                	"gmp_ww/bottle"
	"=Igniter"                	"gmp_ww/Igniter"
	"=Melon"                	"gmp_ww/Melon"
	"Propane Tank"			"models/props_junk/propane_tank001a.mdl"
	"Explosive Barrel"                	"models/props_c17/oildrum001_explosive.mdl"
	"Gas can"                	"models/props_junk/gascan001a.mdl"

	"~Wood"	""
	"Armchair"		"models/props_c17/FurnitureArmchair001a.mdl"
	"Barricade 1"		"models/props_wasteland/barricade001a.mdl"
	"Barricade 2"		"models/props_wasteland/barricade002a.mdl"
	"Board 1"		"models/props_debris/wood_board01a.mdl"
	"Board 2"		"models/props_debris/wood_board02a.mdl"
	"Board 3"		"models/props_debris/wood_board03a.mdl"
	"Board 4"		"models/props_debris/wood_board04a.mdl"
	"Board 5"		"models/props_debris/wood_board05a.mdl"
	"Board 6"		"models/props_debris/wood_board06a.mdl"
	"Board 7"		"models/props_debris/wood_board07a.mdl"
	"Cabinetdrawer 1"		"models/props_interiors/Furniture_CabinetDrawer01a.mdl"
	"Chair 1"		"models/props_c17/FurnitureChair001a.mdl"
	"Chair 1"		"models/props_interiors/Furniture_chair01a.mdl"
	"Couch 1"		"models/props_interiors/Furniture_Couch01a.mdl"
	"Couch 2"		"models/props_interiors/Furniture_Couch02a.mdl"
 	"Crate 1"                	"models/props_junk/wood_crate001a.mdl"
	"Crate 2"                	"models/props_junk/wood_crate002a.mdl"
	"Cupboard"		"models/props_c17/FurnitureCupboard001a.mdl"
	"Desk"		"models/props_interiors/Furniture_Desk01a.mdl"
	"Dock Plank"                		"models/props_wasteland/dockplank01b.mdl"
	"Drawer 1"		"models/props_c17/FurnitureDrawer001a.mdl"
	"Drawer 2"		"models/props_c17/FurnitureDrawer002a.mdl"
	"Drawer 3"		"models/props_c17/FurnitureDrawer003a.mdl"
	"Dresser"		"models/props_c17/FurnitureDresser001a.mdl"
	"Fence 1"		"models/props_wasteland/wood_fence01a.mdl"
	"Fence 2"		"models/props_wasteland/wood_fence02a.mdl"
	"Fence 3"		"models/props_wasteland/wood_fence01b.mdl"
	"Fence 4"		"models/props_wasteland/wood_fence01c.mdl"
	"Pallet"                	"models/props_junk/wood_pallet001a.mdl"
	"Shelf 1"		"models/props_c17/FurnitureShelf001a.mdl"
	"Shelf 2"		"models/props_c17/FurnitureShelf001b.mdl"
	"Shelf 3"		"models/props_wasteland/prison_shelf002a.mdl"
	"Shelf 4"		"models/props_interiors/Furniture_shelf01a.mdl"
	"Selfunit"		"models/props_c17/shelfunit01a.mdl"
	"Seesaw seat"		"models/props_c17/playground_teetertoter_seat.mdl"
	"Swingseat"		"models/props_c17/playground_swingset_seat01a.mdl"
	"Table 1"		"models/props_c17/FurnitureTable001a.mdl"
	"Table 2"		"models/props_c17/FurnitureTable002a.mdl"
	"Table 3"		"models/props_c17/FurnitureTable003a.mdl"

	"~Misc"	""
	"Cinderblock"		"models/props_junk/CinderBlock01a.mdl"
	"Concreteblock"		"models/props_debris/concrete_cynderblock001.mdl"
	"Glass Bottle 1"		"models/props_junk/GlassBottle01a.mdl"
	"Glass Bottle 2"		"models/props_junk/garbage_glassbottle001a.mdl"
	"Glass Bottle 3"		"models/props_junk/garbage_glassbottle002a.mdl"
	"Glass Bottle 4"		"models/props_junk/garbage_glassbottle003a.mdl"
	"Glass Jug"		"models/props_junk/glassjug01.mdl"
	"Sink"		"models/props_wasteland/prison_sink001b.mdl"
	"Toilet"		"models/props_wasteland/prison_toilet01.mdl"
	"Watermelon"				"models/props_junk/watermelon01.mdl"
}
