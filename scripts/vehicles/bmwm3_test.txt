// This is a test jeep type vehicle for the wasteland

"vehicle"
{
	"wheelsperaxle"	"2"
	"body"
	{
		"countertorquefactor"	"1.0"
		"massCenterOverride"	"0 0 15"
		"massoverride"		"1200"		// kg
		"addgravity"		"0.33"
	}
	"engine"
	{
		"horsepower"		"350"
		"maxrpm"		"3000"
		"maxspeed"		"85"		// mph
		"maxReverseSpeed"	"20"		// mph
		"autotransmission"	"1"
		"axleratio"		"3.00"
		"gear"			"2.5"		// 1st gear
		"gear"			"1.59"		// 2nd gear
		"gear"			"1.17"		// 3rd gear
		"gear"			"1.0"		// 4th gear
		"gear"			"0.24"		// 5th gear
		"shiftuprpm"		"1500"
		"shiftdownrpm"		"300"
	
		"boost"
		{
//			"force"		"1.1"	// 1.5 car body mass * gravity * inches / second ^ 2
			"duration"	"2.0"	// 1 second of boost
			"delay"		"15"	// 15 seconds before you can use it again
			"torqueboost"	"1.1"	// enable "sprint" mode of vehicle, not	force type booster			
			"maxspeed"	"125"	// maximum turbo speed
			"force"		"1.5"	// use for value as a boost factor
		}
	}
	"steering"
	{
		"degrees"		"25"
		"fastdampen"		"0.35"
		"slowcarspeed"		"0"
		"fastcarspeed"		"80"
		"slowsteeringrate"	"1"		
		"faststeeringrate"	"0.2"
		"steeringRestFactor"	"1.0"
		"turnbrake"		"1.0"

		"skidallowed"		"0"
		"dustcloud"		"1"
	}

	// front axle
	"axle"
	{
		"wheel"
		{
			"radius"	"16"
			"mass"		"100"
			"inertia"	"0.7"		// steady the car (fixes the oscillation of the axles about z)
			"damping"	"0"
			"rotdamping"	"0.2"
			"material"	"jeeptire"
			"skidmaterial"	"jeeptire"
			"brakematerial" "jeeptire"
		}
		"suspension"
		{
			"springConstant"		"80"
			"springDamping"			"4"
			"stabilizerConstant"		"110"
			"springDampingCompression"	"4"
			"maxBodyForce"			"550"
		}

		"torquefactor"	"0.0"
		"brakefactor"	"0.0"
	}

	// rear axle
	"axle"
	{
		"wheel"
		{
			"radius"	"16"
			"mass"		"100"
			"inertia"	"0.7"		// steady the car (fixes the oscillation of the axles about z)
			"damping"	"0"
			"rotdamping"	"0.5"
			"material"	"enzotire"
			"skidmaterial"	"enzotire"
			"brakematerial" "enzotire"
		}
		"suspension"
		{
			"springConstant"		"80"
			"springDamping"			"4"
			"stabilizerConstant"		"110"
			"springDampingCompression"	"4"
			"maxBodyForce"			"550"
		}
		"torquefactor"	"1.0"
		"brakefactor"	"0.5"
	}
}

"vehicle_sounds"
{
	// List gears in order from lowest speed to highest speed

	"gear"
	{
		"max_speed"		"0.2"
		"speed_approach_factor" "1.0"
	}

	"gear"
	{
		"max_speed"		"0.4"
		"speed_approach_factor" "0.07"
	}
	"gear"
	{
		"max_speed"		"0.6"
		"speed_approach_factor" "0.07"
	}
	"gear"
	{
		"max_speed"		"0.80"
		"speed_approach_factor" "0.035"
	}
	"gear"
	{
		"max_speed"		"0.85"
		"speed_approach_factor" "0.015"
	}
	"gear"
	{
		"max_speed"		"2.0"
		"speed_approach_factor" "0.03"
	}
	"state"
	{
		"name"		"SS_START_WATER"
		"sound"		"socar4_start_in_water"
	}

	"state"
	{
		"name"		"SS_START_IDLE"
		"sound"		"socar4_engine_start"
	}
	"state"
	{
		"name"		"SS_SHUTDOWN_WATER"
		"sound"		"socar4_stall_in_water"
	}
	"state"
	{
		"name"		"SS_IDLE"
		"sound"		"socar4_engine_idle"
	}
	"state"
	{
		"name"		"SS_REVERSE"
		"sound"		"socar4_reverse"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_0"
		"sound"		"socar4_rev"
		"min_time"	"0.75"
	}
	"state"
	{
		"name"		"SS_GEAR_0_RESUME"
		"sound"		"socar4_engine_idle"
		"min_time"	"0.75"
	}
	"state"
	{
		"name"		"SS_GEAR_1"
		"sound"		"socar4_firstgear"
		"min_time"	"1.25"
	}
	"state"
	{
		"name"		"SS_GEAR_1_RESUME"
		"sound"		"socar4_firstgear"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_2"
		"sound"		"socar4_secondgear"
		"min_time"	"1.25"
	}
	"state"
	{
		"name"		"SS_GEAR_2_RESUME"
		"sound"		"socar4_secondgear"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_3"
		"sound"		"socar4_thirdgear"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_3_RESUME"
		"sound"		"socar4_thirdgear"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_4"
		"sound"		"socar4_fourthgear"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_4_RESUME"
		"sound"		"socar4_fourthgear"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_SLOWDOWN_HIGHSPEED"
		"sound"		"socar4_throttleoff_fastspeed"
	}
	"state"
	{
		"name"		"SS_SLOWDOWN"
		"sound"		"socar4_throttleoff_slowspeed"
	}
	"state"
	{
		"name"		"SS_TURBO"
		"sound"		"socar4_turbo_on"
		"min_time"	"2.5"
	}
	"state"
	{
		"name"		"SS_SHUTDOWN"
		"sound"		"socar4_engine_stop"
	}
	"crashsound"
	{
		"min_speed"			"350"
		"min_speed_change"	"250"
		"sound"				"ATV_impact_medium"
		"gear_limit"		"1"
	}
	"crashsound"
	{
		"min_speed"			"450"
		"min_speed_change"	"350"
		"sound"				"ATV_impact_heavy"
	}

	
	"skid_lowfriction"		"ATV_skid_lowfriction"
	"skid_normalfriction"	"ATV_skid_normalfriction"
	"skid_highfriction"		"ATV_skid_highfriction"
}
