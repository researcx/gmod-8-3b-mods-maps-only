// This is the enzo

"vehicle"
{
	"wheelsperaxle"	"2"
	"body"
	{
		"countertorquefactor"	"1.0"
		"massCenterOverride"	"0 0 15"
		"massoverride"		"700"		// kg
		"addgravity"		"0.5"
	}
	"engine"
	{
		"horsepower"		"500"
		"maxrpm"		"3000"
		"maxspeed"		"85"		// mph
		"maxReverseSpeed"	"20"		// mph
		"autotransmission"	"1"
		"axleratio"		"3.00"
		"gear"			"2.5"		// 1st gear
		"gear"			"1.59"		// 2nd gear
		"gear"			"1.17"		// 3rd gear
		"gear"			"1.0"		// 4th gear
		"gear"			"0.24"		// 5th gear
		"shiftuprpm"		"1500"
		"shiftdownrpm"		"300"
	
		"boost"
		{
//			"force"		"1.2"	// 1.5 car body mass * gravity * inches / second ^ 2
			"duration"	"1.0"	// 1 second of boost
			"delay"		"15"	// 15 seconds before you can use it again
			"torqueboost"	"8"	// enable "sprint" mode of vehicle, not	force type booster			
			"maxspeed"	"155"	// maximum turbo speed
			"force"		"1.2"	// use for value as a boost factor
		}
	}
	"steering"
	{
		"degrees"		"1"
		"fastdampen"		"0.25"
		"slowcarspeed"		"0"
		"fastcarspeed"		"80"
		"slowsteeringrate"	"1"		
		"faststeeringrate"	"0.02"
		"steeringRestFactor"	"0.0"
		"turnbrake"		"0.0"

		"skidallowed"		"1"
		"dustcloud"		"1"
	}

	// front axle
	"axle"
	{
		"wheel"
		{
			"radius"	"16"
			"mass"		"100"
			"inertia"	"0.7"		// steady the car (fixes the oscillation of the axles about z)
			"damping"	"0"
			"rotdamping"	"0.0"
			"material"	"jeeptire"
			"skidmaterial"	"jeeptire"
			"brakematerial" "jeeptire"
		}
		"suspension"
		{
			"springConstant"		"80"
			"springDamping"			"4"
			"stabilizerConstant"		"10"
			"springDampingCompression"	"2"
			"maxBodyForce"			"550"
		}

		"torquefactor"	"1.0"
		"brakefactor"	"0.5"
	}

	// rear axle
	"axle"
	{
		"wheel"
		{
			"radius"	"16"
			"mass"		"100"
			"inertia"	"1.5"		// steady the car (fixes the oscillation of the axles about z)
			"damping"	"0"
			"rotdamping"	"0.0"
			"material"	"enzotire"
			"skidmaterial"	"enzotire"
			"brakematerial" "enzotire"
		}
		"suspension"
		{
			"springConstant"		"80"
			"springDamping"			"4"
			"stabilizerConstant"		"10"
			"springDampingCompression"	"2"
			"maxBodyForce"			"550"
		}
		"torquefactor"	"0.0"
		"brakefactor"	"0.5"
	}
}

"vehicle_sounds"
{
	// List gears in order from lowest speed to highest speed

	"gear"
	{
		"max_speed"		"0.3"
		"speed_approach_factor" "1.0"
	}

	"gear"
	{
		"max_speed"		"0.5"
		"speed_approach_factor" "0.07"
	}
	"gear"
	{
		"max_speed"		"0.75"
		"speed_approach_factor" "0.07"
	}
	"gear"
	{
		"max_speed"		"0.80"
		"speed_approach_factor" "0.035"
	}
	"gear"
	{
		"max_speed"		"0.90"
		"speed_approach_factor" "0.015"
	}
	"gear"
	{
		"max_speed"		"1.0"
		"speed_approach_factor" "0.03"
	}
	"state"
	{
		"name"		"SS_START_WATER"
		"sound"		"enzo_start_in_water"
	}

	"state"
	{
		"name"		"SS_START_IDLE"
		"sound"		"enzo_engine_start"
	}
	"state"
	{
		"name"		"SS_SHUTDOWN_WATER"
		"sound"		"enzo_stall_in_water"
	}
	"state"
	{
		"name"		"SS_IDLE"
		"sound"		"enzo_engine_idle"
	}
	"state"
	{
		"name"		"SS_REVERSE"
		"sound"		"enzo_reverse"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_0"
		"sound"		"enzo_rev"
		"min_time"	"0.75"
	}
	"state"
	{
		"name"		"SS_GEAR_0_RESUME"
		"sound"		"enzo_engine_idle"
		"min_time"	"0.75"
	}
	"state"
	{
		"name"		"SS_GEAR_1"
		"sound"		"enzo_firstgear"
		"min_time"	"0.25"
	}
	"state"
	{
		"name"		"SS_GEAR_1_RESUME"
		"sound"		"enzo_firstgear_noshift"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_2"
		"sound"		"enzo_secondgear"
		"min_time"	"0.25"
	}
	"state"
	{
		"name"		"SS_GEAR_2_RESUME"
		"sound"		"enzo_secondgear_noshift"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_3"
		"sound"		"enzo_thirdgear"
		"min_time"	"0.25"
	}
	"state"
	{
		"name"		"SS_GEAR_3_RESUME"
		"sound"		"enzo_thirdgear_noshift"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_4"
		"sound"		"enzo_fourthgear"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_GEAR_4_RESUME"
		"sound"		"enzo_fourthgear_noshift"
		"min_time"	"0.5"
	}
	"state"
	{
		"name"		"SS_SLOWDOWN_HIGHSPEED"
		"sound"		"enzo_throttleoff_fastspeed"
	}
	"state"
	{
		"name"		"SS_SLOWDOWN"
		"sound"		"enzo_throttleoff_slowspeed"
	}
	"state"
	{
		"name"		"SS_TURBO"
		"sound"		"enzo_turbo_on"
		"min_time"	"2.5"
	}
	"state"
	{
		"name"		"SS_SHUTDOWN"
		"sound"		"enzo_engine_stop"
	}
	"crashsound"
	{
		"min_speed"			"350"
		"min_speed_change"	"250"
		"sound"				"ATV_impact_medium"
		"gear_limit"		"1"
	}
	"crashsound"
	{
		"min_speed"			"450"
		"min_speed_change"	"350"
		"sound"				"ATV_impact_heavy"
	}

	
	"skid_lowfriction"		"ATV_skid_lowfriction"
	"skid_normalfriction"	"ATV_skid_normalfriction"
	"skid_highfriction"		"ATV_skid_highfriction"
}
