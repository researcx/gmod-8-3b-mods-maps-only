// "surface group" 
// { 
// "property" 	"value"
// ...
// }
//
// thickness: If this value is present, the material is not volumetrically solid
// it means that the volume should be computed as the surface area times this
// thickness (for automatic mass).  The inside space beneath the thickness value is air.
//
// physics parameters are:
// density: this is the material density in kg / m^3 (water is 1000)
// elasticity: This is the collision elasticity (0 - 1.0, 0.01 is soft, 1.0 is hard)
// friction: this is the physical friction (0 - 1.0, 0.01 is slick, 1.0 is totally rough)
// dampening: this is the physical drag on an object when in contact with this surface (0 - x, 0 none to x a lot)
//
// !!! Do not edit the physics properties (especially density) without the proper references !!!
//
// Sounds
// 
// stepleft: footstep sound for left foot
// stepright: footstep sound for right foot
// impactsoft: Physical impact sound when hitting soft surfaces
// impacthard: Physical impact sound when hitting hard surfaces
// scrapesmooth: Looping physics friction sound (when scraping smooth surfaces)
// scraperough: Looping physics friction sound (when scraping rough surfaces)
// bulletimpact: bullet impact sound
// gamematerial: game material index (can be a single letter or a number)
// 

// NOTE: The properties of "default" will get copied into EVERY material who does not
// 	 override them!!!
//
// "base" means to use the parameters from that material as a base.
// "base" must appear as the first key in a material
//

"default"
{
	"density"	"2500"
	"elasticity"	"0.25"
	"friction"	"0.8"
	"dampening"	"0.0"

	"stepleft"	"Default.StepLeft"
	"stepright"	"Default.StepRight"
	"bulletimpact"	"Default.BulletImpact"
	"scraperough"	"Default.ScrapeRough"
	"scrapesmooth"	"Default.ScrapeSmooth"
	"impacthard"	"Default.ImpactHard"
	"impactsoft"	"Default.ImpactSoft"

	"audioreflectivity" "0.66"
	"audiohardnessfactor" "1.0"
	"audioroughnessfactor" "1.0"

	"scrapeRoughThreshold" "0.5"
	"impactHardThreshold" "0.5"

	"gamematerial"	"C"
	"jumpfactor" "1.0"
	"maxspeedfactor" "1.0"
	"climbable"	"0"
}

// ------------------------------------------
// ---- Gmod Plus
// ------------------------------------------

"gmp_density_high"
{
	"density"	"5000"
}
"gmp_density_normal"
{
	"density"	"2500"
}
"gmp_density_medium"
{
	"density"	"500"
}
"gmp_density_low"
{
	"density"	"100"
}

// Friction

"gmp_friction_super"
{
	"friction"	"999"
}
"gmp_friction_high"
{
	"friction"	"1"
}
"gmp_friction_normal"
{
	"friction"	"0.8"
}
"gmp_friction_medium"
{
	"friction"	"0.4"
}
"gmp_friction_low"
{
	"friction"	"0.01"
}
"gmp_friction_negative"
{
	"friction"	"-0.5"
}

// elasticity

"gmp_bounce_high"
{
	"elasticity"	"9000000"
}
"gmp_bounce_normal"
{
	"elasticity"	"90000"
}
"gmp_bounce_medium"
{
	"elasticity"	"1000"
}
"gmp_bounce_low"
{
	"elasticity"	"0.25"
}

// dampening

"gmp_dampening_high"
{
	"dampening"	"10000"
}
"gmp_dampening_normal"
{

	"dampening"	"1000"
}
"gmp_dampening_medium"
{
	"dampening"	"100"
}
"gmp_dampening_low"
{
	"dampening"	"0.00"
}

"gmp_flubber"
{
	"dampening"	"0.00"
	"elasticity"	"999999999"
	"friction"	"-10"
} 